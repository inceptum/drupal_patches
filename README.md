# Drupal patches

This document contains info about all patches which were applied for current site instance

## List of patches

**List of patches:**

* 2081377.patch

[issue link](http://drupal.org/node/2081377)

_Patch fixes issue with entity connect module. It allows user to integrate long named field collection items with entity connect functionality._

* 2039743-1.patch

[issue link](http://drupal.org/node/2039743)

_Patch fixes issue with deleting multichoice question from Quiz module._

* 2080149.patch

[issue link](http://drupal.org/node/2080149)

_Patch fixes navbar js issues, for properly menu items selection._

* 2080225.patch

[issue link](http://drupal.org/node/2080225)

_Patch provides integration between entity connect and edit modules._

* 1489692-7.patch

[issue link](https://drupal.org/node/1489692)

_Patch fixes incorrect handling of file upload limit exceeded message._

* 1904036-1.patch

[issue link](https://drupal.org/node/1904036)

_Patch fixes incorrect behaviour of field collection when editing/creating nodes(node's title isn't saved after field collection editing )._

* 2092789.patch

[issue link](https://drupal.org/node/2092789)

_Patch provides required option for multichoice title and question fields._

* 1954912.patch

[issue link](https://drupal.org/node/1954912)

_Patch adds navbar's icon for quiz module._

* crossdomain_file.patch

_Patch adds crossdomain xml file for drupal core._

* 1963174-1.patch

[issue link](https://drupal.org/node/1963174)

_Patch fixed issue with facebook login button._